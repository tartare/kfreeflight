<?php
	require_once("./vars.php");

	session_start();
	if(isset($_GET['lang']) && !empty($_GET['lang']))
	{
		$lang = $_GET['lang'];
	}
	else
	{
		if (isset($_SESSION['lang']) && !empty($_SESSION['lang']))
		{
			$lang = $_SESSION['lang'];
		}
		else
		{
			$lang = "en";
		}
	}
	if ((!isset($_SESSION['lang'])) || (empty($_SESSION['lang'])) || ($_SESSION['lang'] != $lang ))
	{
		$_SESSION['lang'] = $lang;
	}

	/*
	// Destroy session
	$_SESSION = array();
	session_destroy();
	$_SESSION['lang'] = $lang;
	*/

	if( isset( $_GET["lang"] ) && !empty( $_GET["lang"] ) )
	{
		$lang = $_GET["lang"];
	}
	else
	{
		$lang = "en";
	}

	$language["code"] = $lang;
	switch ( $lang )
	{
		case "fr": { $language["locale"] = "fr_FR"; break; }
		case "it": { $language["locale"] = "it_IT"; break; }
		case "de": { $language["locale"] = "de_DE"; break; }
		default :  { $language["locale"] = "en_EN";        }
	}

	setlocale( LC_ALL, $language["locale"] );
	bindtextdomain( "kfreeflight", "./locales" );
	textdomain( "kfreeflight" );

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>KFreeFlight the FlightGear GUI for KDE</title>
	<meta name="AUTHOR" content="Didier Fabert">
	<meta http-equiv="Content-Script-Type" content="text/javascript">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="./kfreeflight.css">
	<link rel="icon" type="image/x-icon" href="favicon.ico">
	<script type="text/javascript" src="./js/jquery-1.3.2.min.js"> </script>
	<script type="text/javascript" src="./js/jquery.li-scroller.1.0.js"> </script>
	<script type="text/javascript" src="./js/jquery.easydrag.handler.beta2.js"> </script>
	<script type="text/javascript" src="./js/kfreeflight.js"> </script>
	<script type="text/javascript">
		var manualind = 0;
		var totalmanual = 0;
		var totaltab = <?php echo count( $titlelist ); ?>;
		var manualtab = <?php echo $currentmanualpage; ?>;
		var txtsize = 0;
		var subtitlesize = 0;
		var tabmenusize = 0;
		var shower = 0;
		var showhidemes = new Array();
		showhidemes[0] = <?php echo "'".$message[0]."'"; ?>;
		showhidemes[1] = <?php echo "'".$message[1]."'"; ?>;
		window.onload = function()
		{
			initSubtitle();
			initzoom();
			showTab(manualtab);
		}
	</script>
</head>
<body>
	<?php
		print_header();
		echo "<div class=\"main\">\n";
			print_lang( getLangList(), $language["code"] );
			print_ticker( getTickerList() );
			print_menu( $titlelist );
			for ( $i = 0 ; $i < count( $titlelist ) ; ++$i )
			{
				print_tab( $i, get_include_contents( $titlelist[$i]["page"] ) );
			}
		?>
		<div class="logos">
			<a href="http://sourceforge.net/projects/kfreeflight" target="_blank">
				<img src="http://sflogo.sourceforge.net/sflogo.php?group_id=157130&amp;type=11" width="120" height="31"
					alt="Get KFreeFlight at SourceForge.net. Fast, secure and Free Open Source software downloads">
			</a>
			<a href="http://validator.w3.org/check?uri=referer" target="_blank">
				<img src="http://www.w3.org/Icons/valid-html401"
					alt="Valid HTML 4.01 Transitional" height="31" width="88">
			</a>
			<a href="http://jigsaw.w3.org/css-validator/check/referer" target="_blank">
				<img style="border:0;width:88px;height:31px"
					src="http://jigsaw.w3.org/css-validator/images/vcss" alt="CSS Valide !" />
			</a>
		</div>
	</div>
</body>
</html>