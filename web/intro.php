<?php
$text = "<div class=\"inside\">
<div class=\"textinside\">
<img src=\"./img/tux.png\" width=\"150\" height=\"150\" alt=\"Tux : Linux inside\">
<h3>"._("What is it ?")."</h3><a name=\"whatisit\">
<p>
"._("KFreeFlight is a FlightGear GUI-frontend designed for KDE users. It can launch FlightGear with the most options without writting a very long command line.\n
It was made for GNU/Linux, but it may run on other *nix-like system (not-tested yet).")."
</p>
<p>
"._("This application is integrated as well as possible on the KDE environment, so KDE dependency will never be removed.")."
</p>
<p>
"._("A new version (for KDE4) is in the pipe, but no release was done yet. This version is only available with the subversion tool (SVN). This version is operationnal but a lot of work must be done before a release.")."
</p>
<h3>"._("Disclaimer")."</h3><a name=\"disclaimer\">
<p>
"._("All KFreeFlight tools musn't be use for real navigation. This application is made only for entertainment.")."
</p>
<p>
<strong>"._("Using KFreeFlight for aviation navigation is in fact illegal.")."</strong>
</p>
<h3>"._("License")."</h3><a name=\"license\">
<p>
"._("KFreeFlight is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 2.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License (COPYING file) for more details.")."
</p>
<p>
"._("Actually, the application is available in languages :")."
</p>
<ul>
<li>"._("English")."</li>
<li>"._("French")."</li>
<li>"._("Italian")."</li>
<li>"._("German")."</li>
</ul>
<h3>"._("Thanks to")."</h3><a name=\"thanks\">
<ul>
<li>"._("Sourceforge which hosted KFreeFlight freely.")."</li>
<li>"._("G&eacute;rard for his help, his ideas and his aircraft models.")."</li>
<li>"._("Andrea who make italian translation and Slackware Package.")."</li>
<li>"._("Mauro who make gentoo package.")."</li>
</ul>
<p>"._("If you think that you must be present in this part").",&nbsp;<a href=\"#\" onClick=\"javascript:manualtab=3;showTab(manualtab);\">"._("contact the team")."</a>.</p>
</div>
</div>";
echo $text;
?>