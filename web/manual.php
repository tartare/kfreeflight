<?php

	function get_number_of_page()
	{
		$manualpagelist = get_manual_page_list();
		return ( count( $manualpagelist ) );
	}

	function get_manual_page_list()
	{
		$healthy = array( "[p]", "[/p]", "[ul]", "[/ul]", "[ol]", "[/ol]", "[item]", "[/item]" );
		$yummy   = array( "<p>", "</p>", "<ul>", "</ul>", "<ol>", "</ol>",  "<li>", "</li>" );
		$manualpagelist = array();
		$manualpagelist[0]["title"] = _("Common Options");
		$manualpagelist[0]["type"] = "app";
		$manualpagelist[0]["text"] = str_ireplace( $healthy, $yummy, _("
				[p]In common options[/p]\n
				[ul]\n
					[item]Splashscreen : let you see a splashscreen when FlightGear starting.[/item]\n
					[item]Game mode : Launch FlightGear in true fullscreen mode.[/item]\n
					[item]Fullscreen : Launch FlightGear in a maximised window.[/item]\n
					[item]Intro music : let you ear a music at startup.[/item]\n
					[item]Joystick : make joystick the main input device.[/item]\n
					[item]Sound : enable sound in FlightGear.[/item]\n
					[item]Auto-coordination : enable auto-coordination (usefull if you don't have a more than 3 axes joystick).[/item]\n
					[item]Unit meter : The unit are in feet by default but you can overwrite this setting here[/item]\n
				[/ul]\n
				[p]In advanced options[/p]\n
				[ul]\n
					[item]NMEA protocol : usefull option for Atlas which let you see your position in real time.[/item]\n
					[item]http server : Let you change flying values through a web browser. (by default : http://localhost:5500)[/item]\n
					[item]Atlas too : launch Atlas and FlightGear together[/item]\n
					[item]Wireframe and objects wireframe options : let you discover the FlightGear's internal world.[/item]\n
					[item]Stop terrasync : By default, Terrasync will still running when FlightGear stop. You can stop it here.[/item]\n
				[/ul]\n" ) );
		$manualpagelist[1]["title"] = _("Rendering Options");
		$manualpagelist[1]["type"] = "app";
		$manualpagelist[1]["text"] = str_ireplace( $healthy, $yummy, _("
				[p]Rendering options[/p]\n
				[ul]\n
					[item]Specular highlight : Enable specular reflections on textured objects[/item]\n
					[item]Distance attenuation : Enable runway light distance attenuation[/item]\n
					[item]Enhanced lightning : Enable enhanced runway lighting[/item]\n
					[item]Clouds : Enable 2D (flat) cloud layers[/item]\n
					[item]Cloud 3D : Enable 3D (volumetric) cloud layers[/item]\n
					[item]Random objects : Include random scenery objects (buildings, etc.)[/item]\n
					[item]Mouse pointer : Enable extra mouse pointer (i.e. for full screen Voodoo based cards)[/item]\n
					[item]Sky blend : Enable sky blending[/item]\n
					[item]Horizon effect : Enable celestial body growth illusion near the horizon[/item]\n
					[item]Textures : Enable textures[/item]\n
					[item]Lightning : Enable lightning rendering[/item]\n
					[item]Precipitations : Enable precipitations endering[/item]\n
					[item]AI traffic : Enable artificial intelligence traffic[/item]\n
					[item]Wireframe : disable texturing[/item]\n
				[/ul]\n
				[p]Display resolution, shading and shadows[/p]\n
				[ul]\n
					[item]You can choose here the screen resolution and the color depth. This is the place of your personnal screen resolution defined in the settings window.[/item]\n
					[item]Shading : choose the shading level. Smooth is more beautiful, but need more processor ressource.[/item]\n
					[item]Fog : choose the fog level. Nicest is more beautiful, but need more processor ressource.[/item]\n
				[/ul]\n" ) );
		$manualpagelist[2]["title"] = _("Airport Options");
		$manualpagelist[2]["type"] = "app";
		$manualpagelist[2]["text"] = str_ireplace( $healthy, $yummy, _("
				[p]Here is a list of your favorites airports (not all the &quot;apt.dat.gz&quot; file).[/p]
				[p]You can easily add another aiport by clicking on the &quot;add&quot; button after put the ID and the name of airport on the good place.<br />
				You can delete an airport too by clicking on the &quot;delete&quot; button after selected one in the airport list.[/p]
				[p]You can easily add runways for each airport present in the list. Put the runway name and click on the &quot;add&quot; button.<br />
				You can delete an runway too by clicking on the &quot;delete&quot; button after selected one in the runway list.[/p]
				[p]You can make a research in the &quot;apt.dat.gz file&quot; by clicking the &quot;Search Airport&quot; button.<br />
				This action make the popup airport Window appears[/p]
				[p]The last button &quot;Web search&quot; open the web browser (configured in the configuration dialog) at a preselected page.<br />
				Actually, it's working only for french airports[/p]\n" ) );
		$manualpagelist[3]["title"] = _("Carrier Options");
		$manualpagelist[3]["type"] = "app";
		$manualpagelist[3]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[4]["title"] = _("Aircraft Options");
		$manualpagelist[4]["type"] = "app";
		$manualpagelist[4]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[5]["title"] = _("Start Position Options");
		$manualpagelist[5]["type"] = "app";
		$manualpagelist[5]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[6]["title"] = _("Date Time Options");
		$manualpagelist[6]["type"] = "app";
		$manualpagelist[6]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[7]["title"] = _("Weather Options");
		$manualpagelist[7]["type"] = "app";
		$manualpagelist[7]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[8]["title"] = _("Flightplan Options");
		$manualpagelist[8]["type"] = "app";
		$manualpagelist[8]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[9]["title"] = _("Network Options");
		$manualpagelist[9]["type"] = "app";
		$manualpagelist[9]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[10]["title"] = _("NAV and COM Options");
		$manualpagelist[10]["type"] = "app";
		$manualpagelist[10]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[11]["title"] = _("Scenario Options");
		$manualpagelist[11]["type"] = "app";
		$manualpagelist[11]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[12]["title"] = _("Property Options");
		$manualpagelist[12]["type"] = "app";
		$manualpagelist[12]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[13]["title"] = _("Personnal Options");
		$manualpagelist[13]["type"] = "app";
		$manualpagelist[13]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[14]["title"] = _("Airport Search Tools");
		$manualpagelist[14]["type"] = "airport";
		$manualpagelist[14]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[15]["title"] = _("World View");
		$manualpagelist[15]["type"] = "world";
		$manualpagelist[15]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[16]["title"] = _("METAR Reader");
		$manualpagelist[16]["type"] = "metarreader";
		$manualpagelist[16]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[17]["title"] = _("METAR Editor");
		$manualpagelist[17]["type"] = "metareditor";
		$manualpagelist[17]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[18]["title"] = _("Flightplan Editor");
		$manualpagelist[18]["type"] = "flightplaneditor";
		$manualpagelist[18]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[19]["title"] = _("KFreeFlight Configuration");
		$manualpagelist[19]["type"] = "config";
		$manualpagelist[19]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[20]["title"] = _("FlightGear Path Configuration");
		$manualpagelist[20]["type"] = "config";
		$manualpagelist[20]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[21]["title"] = _("FlightGear Configuration");
		$manualpagelist[21]["type"] = "config";
		$manualpagelist[21]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[22]["title"] = _("Atlas Configuration");
		$manualpagelist[22]["type"] = "config";
		$manualpagelist[22]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[23]["title"] = _("Advanced Configuration");
		$manualpagelist[23]["type"] = "config";
		$manualpagelist[23]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[24]["title"] = _("KFreeFlight Calculator");
		$manualpagelist[24]["type"] = "calc";
		$manualpagelist[24]["text"] = str_ireplace( $healthy, $yummy, _("") );
		$manualpagelist[25]["title"] = _("KFreeFlight Log Window");
		$manualpagelist[25]["type"] = "log";
		$manualpagelist[25]["text"] = str_ireplace( $healthy, $yummy, _("") );
		return $manualpagelist;
	}

	function get_manual_drag_javascript()
	{
		$manualpagelist = get_manual_page_list();
		$text = "";
		for( $i = 0 ; $i < count( $manualpagelist ) ; ++$i )
		{
			$text .= "$(function(){ $('#".$manualpagelist[$i]["type"].($i + 1)."').easydrag(); $('#".$manualpagelist[($i - 1)]["type"].$i."').ondrop(function(e, element){ alert(element + ' Dropped'); }); });\n";
		}
		return $text;
	}

	function get_manual_page( $it )
	{
		$manualpagelist = get_manual_page_list();
		$page = "<div id=\"manual".$it."\" class=\"textinside\">\n";
		$page .= "<a href=\"#\" onClick=\"javascript:togglePopup($('#".$manualpagelist[($it-1)]["type"].$it."'), 0);\">\n";
		$page .= "<span class=\"zoom\">\n";
		$page .= "<span class=\"thumbcontainer\">\n";
		$page .= "<img src=\"./img/thumbnails/screenshot".$it.".jpeg\" class=\"allthumb ".$manualpagelist[($it-1)]["type"]."thumb\" alt=\"KFF screenshot\">\n";
		$page .= "</span>\n";
		$page .= "Click to enlarge&nbsp;\n";
		$page .= "<img src=\"./img/zoom-in.png\" class=\"zoom\" alt=\"Zoom\">\n";
		$page .= "</span>\n";
		$page .= "</a>\n";
		$page .= "<h4>".$manualpagelist[($it-1)]["title"]."</h4>\n";
		$page .= $manualpagelist[($it-1)]["text"]."\n";
		$page .= "<div class=\"viewercontainer ".$manualpagelist[($it-1)]["type"]."view\" id=\"".$manualpagelist[($it-1)]["type"].$it."\">\n";
		$page .= "<div class=\"viewer\">\n";
		$page .= "<h1>".$manualpagelist[($it-1)]["title"]."</h1>\n";
		$page .= "<span class=\"windowbtn\">\n";
		$page .= "<a onMouseOver=\"javascript:showTooltip($('#close".$it."'));\" onMouseOut=\"javascript:$('#close".$it."').hide();\" onClick=\"javascript:togglePopup($('#".$manualpagelist[($it-1)]["type"].$it."'),1);\"><img src=\"./img/fileclose.png\" alt=\"close\"></a>\n";
		$page .= "<a onMouseOver=\"javascript:$('#".$manualpagelist[($it - 1)]["type"].$it."').dragOn();$('.windowbtn').css('cursor', 'move');showTooltip($('#move".$it."'));\" onMouseOut=\"javascript:$('#".$manualpagelist[($it - 1)]["type"].$it."').dragOff();$('.windowbtn').css('cursor', 'default');$('#move".$it."').hide();\" onClick=\"\"><img src=\"./img/move.png\" alt=\"move\"></a>\n";
		$page .= "<div class=\"viewercontainer titlebarview\" id=\"close".$it."\">\n";
		$page .= "<div class=\"viewer\">\n";
		$page .= "<p>Close : Click to close the pop-up window</p>\n";
		$page .= "</div>\n";
		$page .= "</div>\n";
		$page .= "<div class=\"viewercontainer titlebarview\" id=\"move".$it."\">\n";
		$page .= "<div class=\"viewer\">\n";
		$page .= "<p>Move : Press the left button and move the pop-up window and release it when done.</p>\n";
		$page .= "</div>\n";
		$page .= "</div>\n";
		$page .= "</span>\n";
		$page .= "<img src=\"./img/screenshot".$it.".jpeg\" class=\"".$manualpagelist[($it-1)]["type"]."view\" alt=\"KFF screenshot\">\n";
		//$page .= "<p><a onClick=\"javascript:$('#".$manualpagelist[($it-1)]["type"].$it."').hide();\">Click here to close it</a></p>\n";
		$page .= "<p>&nbsp;</p>\n";
		$page .= "</div>\n";
		$page .= "</div>\n";
		$page .= "</div>\n";
		$page .= "<script>\n";
		$page .= "$(function(){ $('#".$manualpagelist[($it - 1)]["type"].$it."').easydrag(); $('#".$manualpagelist[($it - 1)]["type"].$it."').dragOff();/*$('#".$manualpagelist[($i - 1)]["type"].$i."').ondrop(function(e, element){ alert(element + ' Dropped'); });*/});\n";
		$page .= "</script>\n";
		return $page;
	}
				echo "<div class=\"viewercontainer langview\" id=\"lang".$it."\">\n";
				echo "<div class=\"viewer\">\n";
				echo "<p>".$langlist[$it]["text"]."</p>\n";
				echo "</div>\n";
				echo "</div>\n";

	function get_manual()
	{
		$manualpagelist = get_manual_page_list();
		$page = "<div class=\"thumbnail\" id=\"manual0\">\n";
		$page .= "<div class=\"textinside\"><h3>Click on thumbnail to open the page</h3></div>\n";
		//$page .= "<ul class=\"roundabout\">\n";
		for( $i = 1 ; $i <= count ( $manualpagelist ) ; $i++)
		{
			//$page .=  "<li id=\"roundabout".$i."\">\n";
			$page .=  "<div class=\"nothingbutleft\">\n";
			$page .=  "<a href=\"#\" onClick=\"javascript:manualind=".$i.";showManual(manualind);\">\n";
			$page .= "<span class=\"thumbcontainer\">\n";
			$page .= "<img src=\"./img/thumbnails/screenshot".$i.".jpeg\" class=\"allthumb ".$manualpagelist[($i-1)]["type"]."thumb\" alt=\"KFF thumbnail\">\n";
			$page .= "</span>\n";
			$page .= "</a>\n";
			$page .=  "<p class=\"thumb\">".$manualpagelist[($i-1)]["title"]."</p>\n";
			$page .=  "</div>\n";
			//$page .=  "</li>\n";
		}
		//$page .= "</ul>\n";
		$page .= "</div>\n";
		$page .= "<div class=\"inside\" id=\"manualcontainer\">\n";
		for( $i = 1 ; $i <= count ($manualpagelist) ; ++$i )
		{
			$page .= get_manual_page( $i );
		}
		$page .= "<div class=\"cleared\">\n";
		$page .= "<span class=\"nothing\"><a href=\"#\" onClick=\"javascript:manualind=0;manualTop();\"><img src=\"./img/go-up.png\" alt=\"&nbsp;top&nbsp;\"></a></span>\n";
		$page .= "<span class=\"nothing\" id=\"nextenable\"><a href=\"#\" onClick=\"javascript:manualNext();\"><img src=\"./img/go-next.png\" alt=\"&nbsp;&lt;\"></a></span>\n";
		$page .= "<span class=\"nothing\" id=\"nextdisable\"><img src=\"./img/go-next-wb.png\" width=\"32\" height=\"32\" alt=\"&nbsp;&lt;\"></span>\n";
		$page .= "<span class=\"nothing\" id=\"prevenable\"><img src=\"./img/go-previous-wb.png\" width=\"32\" height=\"32\" alt=\"&gt;&nbsp;\"></span>\n";
		$page .= "<span class=\"nothing\" id=\"prevdisable\"><a href=\"#\" onClick=\"javascript:manualPrevious();\"><img src=\"./img/go-previous.png\" alt=\"&gt;&nbsp;\"></a></span>\n";
		$page .= "</div>\n";
		$page .= "</div>\n";
		$page .= "<script type=\"text/javascript\">totalmanual = ".get_number_of_page()."</script>\n";

		return $page;
	}

	echo get_manual();

?>