<?php
echo "<div class=\"inside\">
<div class=\"textinside\">
<h3>"._("Sources tarball")."</h3>
<p>"._("You can find it at")."&nbsp;<a href=\"http://sourceforge.net/project/showfiles.php?group_id=157130\" target=\"new\">"._("Sourceforge release repository")."</a></p>
<a href=\"#\" onClick=\"javascript:manualNext();\"></a>
<h4><a onClick=\"javascript:toggle('#version', '#versiontxt', '#versionup', '#versiondown');\">
<span id=\"versiondown\"><img src=\"./img/view-refresh.png\" alt=\"show\"></span>
<span id=\"versionup\" style=\"display : none;\"><img src=\"./img/view-refresh.png\" alt=\"hide\"></span>
</a><span id=\"versiontxt\">Show version</span></h4>
<div id=\"version\" class=\"tabcontent bordered\">
<p>"._("Upper version")."</p>
<ul>
<li>"._("Dev version are available via SVN")."</li>
<li>"._("Dev version are available via CVS (deprecated now)")."</li>
</ul>
<hr>
<p>"._("Ver 0.3.2-r2 Updated Dev release (2006-04-19)")."</p>
<ul>
<li><strong>"._("Critic bugs fix")."</strong></li>
</ul>
<hr>
<p>"._("Ver 0.3.2 Updated Dev release (2006-04-16)")."</p>
<ul>
<li><strong>"._("Need zlib, glut, plib-1.8.4 and simgear-0.3.10 libraries to compile")."</strong></li>
<li><strong>"._("KWeather and KWorldWatch (all in kde-toys package) must be installed")."</strong></li>
<li>"._("Add Aircraft 3D render thumbnail (by plib or simgear)")."</li>
<li>"._("Add KDE system tray integration option")."</li>
<li>"._("Fix some bugs")."</li>
</ul>
<hr>
<p>"._("Ver 0.3.1 Updated Dev release (2006-03-23)")."</p>
<ul>
<li><strong>"._("Need zlib library to compile")."</strong></li>
<li><strong>"._("KWeather and KWorldWatch (all in kde-toys package) must be installed")."</strong></li>
<li>"._("Add real weather window : download the metar and make a report")."</li>
<li>"._("Add Find an airport by ICAO code, by name or coordinates window in the apt.dat.gz file and KWeather database")."</li>
<li>"._("Add personnal screen resolution (settable by the settings menu)")."</li>
<li>"._("Update multiplayer options")."</li>
<li>"._("Add lightning and precipitation rendering option")."</li>
</ul>
<hr>
<p>"._("Ver 0.2.1 - Updated Stable release (2006-03-01)")."</p>
<ul>
<li>"._("Add shadows(aircraft, ai objects, scenery objects) options")."</li>
<li>"._("Add bump mapped clouds option")."</li>
<li>"._("Add reload aircraft list button")."</li>
<li>"._("Add ATC-Chatter option (FlightGear CVS)")."</li>
</ul>
<hr>
<p>Ver 0.2 - First Stable release (2006-02-07)</p>
<ul>
<li>Adding GPS starting position</li>
<li>Adding french language</li>
<li>Fixing locale bug (locale with comma decimal point)</li>
<li>Adding refresh button in toolbar</li>
</ul>
<hr>
<p>Ver 0.1.4 - Dev release (2006-02-07)</p>
<ul>
<li>More than one config can be saved</li>
<li>Adding a toolbar and a statusbar</li>
<li>Carrier is now working</li>
<li>You can choose your FlightGear executable file and pass it personnal parameters</li>
<li>Adding Comm tab (in use and standby)</li>

<li>Update Radio tab with NAV and ADF standby and adding TACAN</li>
<li>Adding season (summer - winter)</li>
<li>Adding Atlas settings</li>
</ul>
<hr>
<p>Ver 0.1.2 - First Dev release (2006-01-18)</p>
<h4><a onClick=\"javascript:toggle('#version', '#versiontxt', '#versionup', '#versiondown');\">
<span><img src=\"./img/view-refresh.png\" alt=\"hide\"></span>
</a><span>Hide version</span></h4>
</div>
<h3>By distribution</h3>
<p><strong>Don't have any hesitation, make package for your favorite GNU/Linux distribution.</strong></p>
<p>The best way to share it is to upload package on a related website of your favorite distribution.<br />
But don't forget to send us an email, and we put its path on the web site.</p>

<p><strong>Available</strong></p>
<ul>
<li>Slackware package at
<a href=\"http://www.slacky.it/index.php?option=com_remository&amp;Itemid=1&amp;func=fileinfo&amp;filecatid=922&amp;parent=category\" target=\"new\">
www.slacky.it</a> making by Andrea.</li>
<li>Gentoo ebuild at <a href=\"http://bugs.gentoo.org/show_bug.cgi?id=125381\" target=\"new\">
bugs.gentoo.org</a> making by Mauro.</li>
<li> Suse 10.0 rpm package on <a href=\"http://home.tiscali.be/raoul.linux/downloadSuSE10.0.htm\" target=\"new\">home.tiscali.be/raoul.linux</a> making by Raoul.</li>
</ul>

<h3>SVN Version</h3>
<h4>Get a local copy</h4>
<p>To check out a new copy of KFreeFlight from Subversion, open a terminal, cd into the directory where you want the new
KFreeFlight directory to be created and run the commands listed below. When prompted for the password, just press
the Enter key. Don't forget to launch \"make install\" as root. The necessary commands are:</p>
<div class=\"cmdline\">$ svn co https://kfreeflight.svn.sourceforge.net/svnroot/kfreeflight kfreeflight<br>
$ cd kfreeflight<br>
$ mkdir build<br>
$ cd build<br>
$ cmake ..<br>
$ make<br>
$ make install</div>
<h4>Update your SVN copy</h4>
<div class=\"cmdline\">$ svn update</div>

<h3><a onClick=\"javascript:toggle('#getcvs', 0, '#cvsup', '#cvsdown');\">
<span id=\"cvsdown\"><img src=\"./img/view-refresh.png\" alt=\"show\"></span>
<span id=\"cvsup\" style=\"display : none;\"><img src=\"./img/view-refresh.png\" alt=\"hide\"></span></a>
CVS Version (deprecated)</h3>
<div id=\"getcvs\" style=\"display : none;\">
<p>A CVS version is now available<br /><a href=\"http://kfreeflight.cvs.sourceforge.net/kfreeflight/\" target=\"new\">
CVS Web browser</a></p>

<h4>Get a local copy</h4>
<p>To check out a new copy of KFreeFlight from CVS, open a terminal, cd into the directory where you want the new
KFreeFlight directory to be created and run the commands listed below. When prompted for the password, just press
the Enter key. Don't forget to launch \"make install\" as root. The necessary commands are:</p>
<div class=\"cmdline\">$ cvs -d:pserver:anonymous@kfreeflight.cvs.sourceforge.net:/cvsroot/kfreeflight login<br>
$ cvs -z3 -d:pserver:anonymous@kfreeflight.cvs.sourceforge.net:/cvsroot/kfreeflight co -P kfreeflight<br>
$ cd kfreeflight<br>
$ gmake -f Makefile.cvs<br>
$ ./configure<br>
$ make<br>
$ make install</div>
<h4>Update your CVS copy</h4>
<div class=\"cmdline\">$ cvs update -dP</div>
</div>
</div>
</div>";