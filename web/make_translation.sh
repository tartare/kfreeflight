#!/bin/bash
 
# get pot file
xgettext -D . -d kfreeflight -L PHP --from-code=iso_8859-15 *.php

# make po to mo file
mkdir -p locales
for KFFLANGUAGE in fr_FR it_IT de_DE
do
	#cp kfreeflight.po locales/${KFFLANGUAGE}/LC_MESSAGES/
	mkdir -p locales/${KFFLANGUAGE}
	mkdir -p locales/${KFFLANGUAGE}/LC_MESSAGES
	msgfmt locales/${KFFLANGUAGE}/LC_MESSAGES/kfreeflight.po -o locales/${KFFLANGUAGE}/LC_MESSAGES/kfreeflight.mo
done
