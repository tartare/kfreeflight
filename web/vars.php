<?php
	/*function getMenuList()
	{*/
		$titlelist = array();
		$subtitlelist = array();

		$menu["title"] = "Home";
		$menu["page"] = "intro.php";
		$subtitlelist = array();
		/*$subtitle["text"] = "What is it ?";
		$subtitle["link"] = "whatisit";
		array_push($subtitlelist, $subtitle);
		$subtitle["text"] = "Disclaimer";
		$subtitle["link"] = "disclaimer";
		array_push($subtitlelist, $subtitle);
		$subtitle["text"] = "License";
		$subtitle["link"] = "license";
		array_push($subtitlelist, $subtitle);
		$subtitle["text"] = "Thanks";
		$subtitle["link"] = "thanks";
		array_push($subtitlelist, $subtitle);*/
		$menu["sub"] = $subtitlelist;
		array_push( $titlelist, $menu );

		$menu["title"] = "Download";
		$menu["page"] = "download.php";
		$subtitlelist = array();
		/*$subtitle["text"] = "Source tarball";
		$subtitle["link"] = "";
		array_push($subtitlelist, $subtitle);
		$subtitle["text"] = "Bin-packages";
		$subtitle["link"] = "";
		array_push($subtitlelist, $subtitle);
		$subtitle["text"] = "SVN";
		$subtitle["link"] = "";
		array_push($subtitlelist, $subtitle);
		$subtitle["text"] = "CVS";
		$subtitle["link"] = "";
		array_push($subtitlelist, $subtitle);*/
		$menu["sub"] = $subtitlelist;
		array_push( $titlelist, $menu );

		$menu["title"] = "FAQ";
		$menu["page"] = "faq.php";
		$subtitlelist = array();
		$menu["sub"] = $subtitlelist;
		array_push( $titlelist, $menu );

		$menu["title"] = "Contact";
		$menu["page"] = "contact.php";
		$subtitlelist = array();
		$menu["sub"] = $subtitlelist;
		array_push( $titlelist, $menu );

		$menu["title"] = "TODO";
		$menu["page"] = "todo.php";
		$subtitlelist = array();
		$menu["sub"] = $subtitlelist;
		array_push( $titlelist, $menu );

		$menu["title"] = "Manual";
		$menu["page"] = "manual.php";
		$subtitlelist = array();
		$menu["sub"] = $subtitlelist;
		array_push( $titlelist, $menu );

		/*$menu["title"] = "Admin";
		$menu["page"] = ".php";
		$subtitlelist = array();
		$menu["sub"] = $subtitlelist;
		array_push( $titlelist, $menu );*/

		//return $titlelist;
	//}

	function getTickerList()
	{
		$ticker = array();
		$tickerlist = array();
		$ticker["date"] = "01/01/2010";
		$ticker["text"] = "A new SVN repository is available ( KDE4 version )";
		array_push( $tickerlist, $ticker );
		$ticker["date"] = "01/01/2010";
		$ticker["text"] = "CVS repository is out of date";
		array_push( $tickerlist, $ticker );
		$ticker["date"] = "01/08/2010";
		$ticker["text"] = "Last website update";
		array_push( $tickerlist, $ticker );

		return $tickerlist;
	}

	function getLangList()
	{
		$langlist = array();
		$lang = array();
		$lang["lang"] = "en";
		$lang["text"] = "English / American language";
		array_push( $langlist, $lang );
		$lang["lang"] = "fr";
		$lang["text"] = "French language";
		array_push( $langlist, $lang );
		$lang["lang"] = "it";
		$lang["text"] = "Italian language";
		array_push( $langlist, $lang );
		$lang["lang"] = "de";
		$lang["text"] = "German language";
		array_push( $langlist, $lang );

		return $langlist;
	}
	function print_lang($langlist, $current)
	{
			echo "<div class=\"language\">\n";
			for ( $it = 0 ; $it < count( $langlist ) ; $it++ )
			{
				if($langlist[$it]["lang"] == $current)
				{
					$size = "bigflag";
				}
				else
				{
					$size = "flag";
				}
				echo "<a class=\"lang\" href=\"./index.php?lang=".$langlist[$it]["lang"]."\" onMouseOver=\"javascript:showTooltip($('#lang".$it."'));\" onMouseOut=\"javascript:$('#lang".$it."').hide();\">\n";
				echo "<img src=\"./img/".$langlist[$it]["lang"].".png\" alt=\"en\" class=\"".$size."\">\n";
				echo "</a>\n";
				echo "<div class=\"viewercontainer langview\" id=\"lang".$it."\">\n";
				echo "<div class=\"viewer\">\n";
				echo "<p>".$langlist[$it]["text"]."</p>\n";
				echo "</div>\n";
				echo "</div>\n";
			}
			/*
			echo "<div>\n";
			echo "<a href=\"#\" onClick=\"javascript:zoom( 2 );\" onMouseOver=\"javascript:showTooltip($('#zoomin'));\" onMouseOut=\"javascript:$('#zoomin').hide();\">\n";
			echo "<img src=\"./img/zoom-in.png\" class=\"iconmedium\" alt=\"Zoom+\">\n";
			echo "</a>\n";
			echo "<div class=\"viewercontainer zoomview\" id=\"zoomin\">\n";
			echo "<div class=\"viewer\">\n";
			echo "<p>Zoom +</p>\n";
			echo "</div>\n";
			echo "</div>\n";
			echo "<a href=\"#\" onClick=\"javascript:zoom( 0 );\" onMouseOver=\"javascript:showTooltip($('#resetzoom'));\" onMouseOut=\"javascript:$('#resetzoom').hide();\">\n";
			echo "<img src=\"./img/zoom-original.png\" class=\"iconmedium\" alt=\"NoZoom\">\n";
			echo "</a>\n";
			echo "<div class=\"viewercontainer zoomview\" id=\"resetzoom\">\n";
			echo "<div class=\"viewer\">\n";
			echo "<p>Reset Zoom</p>\n";
			echo "</div>\n";
			echo "</div>\n";
			echo "<a href=\"#\" onClick=\"javascript:zoom( -2 );\" onMouseOver=\"javascript:showTooltip($('#zoomout'));\" onMouseOut=\"javascript:$('#zoomout').hide();\">\n";
			echo "<img src=\"./img/zoom-out.png\" class=\"iconmedium\" alt=\"Zoom-\">\n";
			echo "</a>\n";
			echo "<div class=\"viewercontainer zoomview\" id=\"zoomout\">\n";
			echo "<div class=\"viewer\">\n";
			echo "<p>Zoom -</p>\n";
			echo "</div>\n";
			echo "</div>\n";
			echo "</div>\n";*/
			echo "</div>\n";
	}

	function print_tab( $it, $text, $manual = false )
	{
		$healthy = array( " KFreeFlight ", " FlightGear ", " KDE " );
		$yummy   = array( " <em>KFreeFlight</em> ", " <em>FlightGear</em> ", " <em>KDE</em> " );
		echo "<div id=\"tab".$it."\" class=\"tabcontent\">\n";
		echo str_ireplace( $healthy, $yummy, $text )."\n";;
		//echo $text."\n";
		echo "</div>\n";
	}

	function print_menu($titlelist)
	{
		echo "<div class=\"menu\">\n";
		for ( $it = 0 ; $it < count( $titlelist ) ; $it++ )
		{
			if ( $it == 1 )
			{
				$act = " activated";
			}
			else
			{
				$act = "";
			}
			echo "<p class=\"tabmenuleft tabmenu\" id=\"ltab".$it."\"></p>";
			echo "<a href=\"#\" class=\"tabmenu".$act."\" onClick=\"javascript:manualtab=".$it.";showTab(manualtab);\" onMouseOver=\"javascript:setTabOver( 1, ".$it." );";
			if ( count( $titlelist[$it]["sub"] ) )
			{
				echo "$('#sub".$it."').show();";
			}
			echo "\" onMouseOut=\"javascript:setTabOver( 0, ".$it." );";
			if ( count( $titlelist[$it]["sub"] ) )
			{
				echo "$('#sub".$it."').hide();";
			}
			echo "\">".$titlelist[$it]["title"]."</a>\n";
			echo "<p class=\"tabmenuright tabmenu\" id=\"rtab".$it."\"></p>";
			for ( $it2 = 0 ; $it2 < count( $titlelist[$it]["sub"] ) ; $it2++ )
			{
				if ( $it2 == 0)
				{
					echo "<div id=\"sub".$it."\" class=\"subtitle\"  onMouseOver=\"javascript:$('#sub".$it."').show();\" onMouseOut=\"javascript:$('#sub".$it."').hide();\" onClick=\"javascript:manualtab=".$it.";showTab(manualtab);\">\n";
					echo "<ul>\n";
				}
				echo "<a href=\"#".$titlelist[$it]["sub"][$it2]["link"]."\"><li>".$titlelist[$it]["sub"][$it2]["text"]."</li></a>\n";
				if ( $it2 == ( count( $titlelist[$it]["sub"] ) -1 ) )
				{
					echo "</ul>\n";
					echo "</div>\n";
				}
			}
		}
		echo "</div>\n";
		echo "<div class=\"menubottom\">\n";
		for ( $it = 0 ; $it < count( $titlelist ) ; $it++ )
		{
			echo "<span class=\"bottommenu bottommenuinactive\" id=\"menu".$it."\"></span>\n";
		}
		echo "</div>\n";
	}

	function print_ticker( $tickerlist )
	{
		echo "<div class=\"ticker\">\n";
		echo "<ul id=\"ticker01\">\n";
		for ( $it = 0 ; $it < count( $tickerlist ) ; $it++ )
		{
			echo "<li>\n";
			echo "<img src=\"./img/plane.png\" alt=\"\" width=\"68\" height=\"18\">\n";
			echo "<span>".$tickerlist[$it]["date"]."</span>\n";
			echo "<a href=\"#\">".$tickerlist[$it]["text"]."</a>\n";
			echo "</li>\n";
		}
		echo "</ul>\n";
		echo "</div>\n";
		echo "<script>$(function(){ $('#ticker01').liScroll() });</script>\n";
	}

	function print_header()
	{
		//echo "<div class=\"header\">\n";
		//echo "<span class=\"headerleft\"></span>\n";
		//echo "<span class=\"headermiddle\"><img src=\"./img/logo.png\" class=\"logo\" >&nbsp;KFreeFlight</span>\n";
		//echo "<span class=\"headerright\"></span>\n";
		//echo "</div>\n";
		echo "<h1 class=\"title\">";
		echo "<img src=\"./img/logo.png\" class=\"biglogo\" >&nbsp;";
		echo "KFreeFlight";
		echo "</h1>\n";
	}

	function get_include_contents($filename)
	{
		if (is_file($filename))
		{
			ob_start();
			include $filename;
			$contents = ob_get_contents();
			ob_end_clean();
			return $contents;
		}
		return false;
	}

	$currentmanualpage = 1;
	$message[0] = _("Show version");
	$message[1] = _("Hide version");
?>