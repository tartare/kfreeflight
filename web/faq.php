<?php

function get_faq()
{
	$faqlist = array();
	$faq = array();

	$faq["question"] = _("How send a bug report if I can't build the application ?");
	$faq["answer"] = _( "Use the Bug Tracking system, there is a link on the \"Contact\" tab" );
	array_push( $faqlist, $faq );

	$faq["question"] = _( "Does KFreeFlight support the nice option for FlightGear executable ?" );
	$faq["answer"] = _( "No. The \"nice -x fgfs\" executable doesn't make aircraft list." );
	array_push( $faqlist, $faq );

	$faq["question"] = _( "Is it possible to remove the KDE dependency which make application more cross-platform?" );
	$faq["answer"] = _( "No it's impossible : KFreeFlight was born because KDE haven't any launcher.\nI don't want to make a clone but offering something different and integrated as well as possible with KDE.\nThen I will NEVER change the code for the Redmond operating system which isn't a friendly platform for Open Source\n\nTHINK DIFFERENT, THINK FREE" );
	array_push( $faqlist, $faq );

	$faq["question"] = _( "Why the 0.3.1 version is in the development branch ?" );
	$faq["answer"] = _( "The application is stable but some features aren't enough tested, especially the weather report which may making a stable but wrong report." );
	array_push( $faqlist, $faq );

	$faq["question"] = _( "Why have I an error when i dezip the tarball ?" );
	$faq["answer"] = _( "You must dezip the tarball with the following command line in a terminal.\n[cmd]tar -xzf kfreeflight-<version>.tar.gz[/cmd]\nYou couldn't using ark or anything else." );
	array_push( $faqlist, $faq );

	$faq["question"] = _( "Why have I some warnings about aclocal ?" );
	$faq["answer"] = _( "This part is in progress and appear after an update of autools. Actually, this isn't a problem for building executable" );
	array_push( $faqlist, $faq );

	$faq["question"] = _( "Why the application's building fail in the linking stage ?" );
	$faq["answer"] = _( "You can try the following command line and relaunch the building process.\n[cmd]gmake -f Makefile.cvs[/cmd]\nIf it's not solve the problem, send a bug report." );
	array_push( $faqlist, $faq );

	$faq["question"] = _( "I can see the splashscreen but the application crash after !" );
	$faq["answer"] = _( "Relaunch the application (this part is in progress).\nSome crash came because of false aircraft xml file (In that case change manually the number of aircraft in the config file :\n/home/<USER>/.kfreeflight/<Current_config_name>" );
	array_push( $faqlist, $faq );

	$faq["question"] = _( "The splashscreen doesn't diseappear ! (v0.3.2 only)" );
	$faq["answer"] = _( "The 0.3.2-r2 release solve this problem." );
	array_push( $faqlist, $faq );

	$faq["question"] = _( "There is no 3D render or thumbnail for the aircraft on the first launch ! (v0.3.2 only)" );
	$faq["answer"] = _( "This is a bug already reported : before i can fix this problem, relaunch the application.\nThe 0.3.2-r2 release correct this problem." );
	array_push( $faqlist, $faq );

	$healthy = array( "[cmd]", "[/cmd]" );
	$yummy   = array( "<div class=\"cmdline\">$ ", "</div>" );

	$text = "<h3>F.A.Q.</h3>\n";
	$text .= "<ul>\n";
	for ( $it = 0 ; $it < count( $faqlist ) ; $it++ )
	{
		$text .= "<li><a href=\"#faq".$it."\">".htmlentities( $faqlist[$it]["question"] )."</a></li>\n";
	}
	$text .= "</ul>\n";
	for ( $it = 0 ; $it < count( $faqlist ) ; $it++ )
	{
		$text .= "<a name=\"faq".$it."\"></a>\n";
		$text .= "<h3>".str_ireplace( $healthy, $yummy, nl2br( htmlentities( $faqlist[$it]["question"] ) ) )."</h3>\n";
		$text .= "<p>".str_ireplace( $healthy, $yummy, nl2br( htmlentities( $faqlist[$it]["answer"] ) ) )."</p>\n";
	}
	return $text;
}
echo "<div class=\"inside\">\n";
echo "<div class=\"textinside\">\n";
echo get_faq();
echo "</div>\n";
echo "</div>\n";

?>