<?php
echo "<div class=\"inside\">
<div class=\"textinside\">
<h3>"._("Mailing List : Devel")."</h3>
<p>"._("This is the good place for report problem (compilation, bugs, etc...), query a new feature or just taking about kfreeflight.")."</p>
<p>"._("Subscribe")."&nbsp;:&nbsp;<br><a href=\"https://lists.sourceforge.net/lists/listinfo/kfreeflight-devel\">"._("Go to Subscribe page")."</a></p>
<p>Archives<br>
<a href=\"http://sourceforge.net/mailarchive/forum.php?forum=kfreeflight-devel\">"._("Archived Messages")."</a></p>
<h3><a name=\"forum\"></a>"._("Forum")."</h3>
<ul>
<li><a href=\"http://sourceforge.net/projects/kfreeflight/forums/forum/528354\">"._("Forum Help")."</a>&nbsp;:&nbsp;"._("Get help on KFreeFlight usage.")."</li>
</ul>
<h3><a name=\"bugreport\"></a>"._("Bug report")."</h3>
<p>"._("The best way to report a bug is to use the bug tracking system. and request new feature to feature requests tracking .\n
But keep in mind that KDE dependency will never be removed.")."</p>
<ul>
<li><a href=\"http://sourceforge.net/tracker/?group_id=157130&atid=802561\">"._("Bug Tracking")."</a></li>
<li><a href=\"http://sourceforge.net/tracker/?group_id=157130&atid=802564\">"._("Feature Requests")."</a></li>
</ul>
<h3><a name=\"links\"></a>"._("Links")."</h3>
<p>"._("Developers web site :")."</p>
<ul>
<li><a href=\"http://didier.fabert.free.fr\">Didier-Linux</a></li>
<li><a href=\"http://perso.orange.fr/GRTux\">G&eacute;rard Robin</a></li>
</ul>
<p>"._("FlightGear web site :")."</p>
<ul>
<li><a href=\"http://www.flightgear.org\">FlightGear</a></li>
<li><a href=\"http://atlas.sourceforge.net\">Atlas</a></li>
</ul>
<p>"._("Interesting applications :")."</p>
<ul>
<li>TaxiDraw</li>
<li>fgsd</li>
</ul>
</div>
</div>";
?>