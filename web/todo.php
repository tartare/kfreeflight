<div class="inside">
<div class="textinside">
<h3>TODO</h3>
<ul>
<li>Correct my spelling errors in application and website.</li>
<li>Translate the new application and website.</li>
</ul>

<h4>Otherwise, it rest a lot of things</h4>
<ul>
<li>Others languages traductions (need help)</li>
<li>Finish and translate this web page (need help for translation)</li>
<li>etc ...</li>
</ul>
<h3>How to translate KFreeFlight ?</h3>
<p>This part is very simple : KFreeFlight put pot file in your \$KDE/share/apps/kfreeflight directory (min version 0.2).<br />
Just open the <em>kfreeflight.pot</em> file with KBabel or another po file editor and rename it with locale and the <em>po</em> extention.<br />

example : for french translation rename the file <em>fr.po</em><br />
Now you can translate the application. Don't forget to send us this file at
didier.fabert[-At-]gmail.com</p>
<p>But it's a good idea to contact us before, in case of someone else do the job. Don't forget to work
with the lastest pot file in development ( taking it by subversion ).</p>
</div>
</div>