# translation of kfreeflight.po to
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Didier Fabert <didier.fabert@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kfreeflight\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2010-02-16 06:49+0100\n"
"PO-Revision-Date: 2010-02-16 06:50+0100\n"
"Last-Translator: Didier Fabert <didier.fabert@gmail.com>\n"
"Language-Team:  <en@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#: contact.php:4
msgid "Mailing List : Devel"
msgstr ""

#: contact.php:5
msgid ""
"This is the good place for report problem (compilation, bugs, etc...), query "
"a new feature or just taking about kfreeflight."
msgstr ""

#: contact.php:6
msgid "Subscribe"
msgstr "Souscrire"

#: contact.php:6
msgid "Go to Subscribe page"
msgstr ""

#: contact.php:8
msgid "Archived Messages"
msgstr ""

#: contact.php:9
msgid "Forum"
msgstr ""

#: contact.php:11
msgid "Forum Help"
msgstr ""

#: contact.php:11
msgid "Get help on KFreeFlight usage."
msgstr ""

#: contact.php:13
msgid "Bug report"
msgstr ""

#: contact.php:14
msgid ""
"The best way to report a bug is to use the bug tracking system. and request "
"new feature to feature requests tracking .\n"
"\n"
"But keep in mind that KDE dependency will never be removed."
msgstr ""

#: contact.php:17
msgid "Bug Tracking"
msgstr ""

#: contact.php:18
msgid "Feature Requests"
msgstr ""

#: contact.php:20
msgid "Links"
msgstr ""

#: contact.php:21
msgid "Developers web site :"
msgstr ""

#: contact.php:26
msgid "FlightGear web site :"
msgstr ""

#: contact.php:31
msgid "Interesting applications :"
msgstr ""

#: faq.php:8
msgid "How send a bug report if I can't build the application ?"
msgstr ""

#: faq.php:9
msgid "Use the Bug Tracking system, there is a link on the \"Contact\" tab"
msgstr ""

#: faq.php:12
msgid "Does KFreeFlight support the nice option for FlightGear executable ?"
msgstr ""

#: faq.php:13
msgid "No. The \"nice -x fgfs\" executable doesn't make aircraft list."
msgstr ""

#: faq.php:16
msgid ""
"Is it possible to remove the KDE dependency which make application more "
"cross-platform?"
msgstr ""

#: faq.php:17
msgid ""
"No it's impossible : KFreeFlight was born because KDE haven't any launcher.\n"
"I don't want to make a clone but offering something different and integrated "
"as well as possible with KDE.\n"
"Then I will NEVER change the code for the Redmond operating system which "
"isn't a friendly platform for Open Source\n"
"\n"
"THINK DIFFERENT, THINK FREE"
msgstr ""

#: faq.php:20
msgid "Why the 0.3.1 version is in the development branch ?"
msgstr ""

#: faq.php:21
msgid ""
"The application is stable but some features aren't enough tested, especially "
"the weather report which may making a stable but wrong report."
msgstr ""

#: faq.php:24
msgid "Why have I an error when i dezip the tarball ?"
msgstr ""

#: faq.php:25
msgid ""
"You must dezip the tarball with the following command line in a terminal.\n"
"[cmd]tar -xzf kfreeflight-<version>.tar.gz[/cmd]\n"
"You couldn't using ark or anything else."
msgstr ""

#: faq.php:28
msgid "Why have I some warnings about aclocal ?"
msgstr ""

#: faq.php:29
msgid ""
"This part is in progress and appear after an update of autools. Actually, "
"this isn't a problem for building executable"
msgstr ""

#: faq.php:32
msgid "Why the application's building fail in the linking stage ?"
msgstr ""

#: faq.php:33
msgid ""
"You can try the following command line and relaunch the building process.\n"
"[cmd]gmake -f Makefile.cvs[/cmd]\n"
"If it's not solve the problem, send a bug report."
msgstr ""

#: faq.php:36
msgid "I can see the splashscreen but the application crash after !"
msgstr ""

#: faq.php:37
msgid ""
"Relaunch the application (this part is in progress).\n"
"Some crash came because of false aircraft xml file (In that case change "
"manually the number of aircraft in the config file :\n"
"/home/<USER>/.kfreeflight/<Current_config_name>"
msgstr ""

#: faq.php:40
msgid "The splashscreen doesn't diseappear ! (v0.3.2 only)"
msgstr ""

#: faq.php:41
msgid "The 0.3.2-r2 release solve this problem."
msgstr ""

#: faq.php:44
msgid ""
"There is no 3D render or thumbnail for the aircraft on the first launch ! "
"(v0.3.2 only)"
msgstr ""

#: faq.php:45
msgid ""
"This is a bug already reported : before i can fix this problem, relaunch the "
"application.\n"
"The 0.3.2-r2 release correct this problem."
msgstr ""

#: index.php:26
msgid "Show version"
msgstr ""

#: index.php:27
msgid "Hide version"
msgstr ""

#: intro.php:5
msgid "What is it ?"
msgstr ""

#: intro.php:7
msgid ""
"KFreeFlight is a FlightGear GUI-frontend designed for KDE users. It can "
"launch FlightGear with the most options without writting a very long command "
"line.\n"
"\n"
"It was made for GNU/Linux, but it may run on other *nix-like system (not-"
"tested yet)."
msgstr ""

#: intro.php:11
msgid ""
"This application is integrated as well as possible on the KDE environment, "
"so KDE dependency will never be removed."
msgstr ""

#: intro.php:14
msgid ""
"A new version (for KDE4) is in the pipe, but no release was done yet. This "
"version is only available with the subversion tool (SVN). This version is "
"operationnal but a lot of work must be done before a release."
msgstr ""

#: intro.php:16
msgid "Disclaimer"
msgstr ""

#: intro.php:18
msgid ""
"All KFreeFlight tools musn't be use for real navigation. This application is "
"made only for entertainment."
msgstr ""

#: intro.php:21
msgid "Using KFreeFlight for aviation navigation is in fact illegal."
msgstr ""

#: intro.php:23
msgid "License"
msgstr ""

#: intro.php:25
msgid ""
"KFreeFlight is free software; you can redistribute it and/or modify it under "
"the terms of the GNU General Public License as published by the Free "
"Software Foundation, version 2.\n"
"This program is distributed in the hope that it will be useful, but WITHOUT "
"ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or "
"FITNESS FOR A PARTICULAR PURPOSE.\n"
"See the GNU General Public License (COPYING file) for more details."
msgstr ""

#: intro.php:30
msgid "Actually, the application is available in languages :"
msgstr ""

#: intro.php:33
msgid "English"
msgstr ""

#: intro.php:34
msgid "French"
msgstr ""

#: intro.php:35
msgid "Italian"
msgstr ""

#: intro.php:36
msgid "German"
msgstr ""

#: intro.php:38
msgid "Thanks to"
msgstr ""

#: intro.php:40
msgid "Sourceforge which hosted KFreeFlight freely."
msgstr ""

#: intro.php:41
msgid "G&eacute;rard for his help, his ideas and his aircraft models."
msgstr ""

#: intro.php:42
msgid "Andrea who make italian translation and Slackware Package."
msgstr ""

#: intro.php:43
msgid "Mauro who make gentoo package."
msgstr ""

#: intro.php:45
msgid "If you think that you must be present in this part,"
msgstr ""

#: intro.php:45
msgid "contact the team"
msgstr ""

